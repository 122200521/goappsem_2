package model

import "myapp/datastore/postgres"

type Admin struct{
	FirstName string
	LastName string
	Email string
	Password string
}

const (queryInsertAdmin = "INSERT INTO admin(firstname,lastname,email,password) VALUES($1,$2,$3,$4);"
 queryGetAdmin = "SELECT email,password From admin WHERE email = $1 and password=$2;"
)

 func (adm *Admin) Create() error {
	_,err := postgres.Db.Exec(queryInsertAdmin, adm.FirstName, adm.LastName,adm.Email,adm.Password)
	return err
}


 func (adm *Admin) Get() error{
    // fmt.Println(s)
err :=postgres.Db.QueryRow(queryGetAdmin,adm.Email,adm.Password).Scan (&adm.Email,&adm.Password)
// we use .Exec when we don't want anything to be returned
return err   
 }
	






