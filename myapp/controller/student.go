package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent (w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){                     //if true this if code won't execute so use !VerifyCookie which is false
		return  
	}
	//fmt.Fprintf(w, "add student handler")
	var stud model.Student
	fmt.Println(stud)
	decoder:=json.NewDecoder(r.Body)
	err:=decoder.Decode(&stud)
	if err !=nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}
    // studPointer:=&stud
	// studpointer.Create()
	dbErr:=stud.Create()
	if dbErr !=nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		return
	}
	httpResp.ResponseWithJson (w, http.StatusCreated, map[string]string{"message": "Student data added"})

	
}
func GetStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){                     //if true this if code won't execute so use !VerifyCookie which is false
		return  
	}
// get url parameter
sid:=mux.Vars(r)["sid"]
stdId,idErr := getUserId(sid)
if idErr !=nil{
	httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
	return
}
s:= model.Student{StdId: stdId}
getErr :=s.Read()
if getErr !=nil{
	switch getErr{
	case sql.ErrNoRows:
		httpResp.RespondWithError(w,http.StatusNotFound,"Student not found")
	default:
		httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())

	}
	return
}
httpResp.ResponseWithJson(w, http.StatusOK, s)
}

// convert string sid to int
func getUserId(userIdParam string)(int64,error){
	userId,userErr := strconv.ParseInt(userIdParam,10,64)
	if userErr != nil{
		return 0, userErr
	}
	return userId, nil

}

func UpdateStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){                     //if true this if code won't execute so use !VerifyCookie which is false
		return  
	}
	old_sid:= mux.Vars(r)["sid"]
	old_stdId, idErr := getUserId(old_sid)
    if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var stud model.Student
	decoder :=json.NewDecoder(r.Body)
	if err := decoder.Decode(&stud); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	err := stud.Update(old_stdId)
	if err != nil{
		switch err{
	        case sql.ErrNoRows:
		        httpResp.RespondWithError(w, http.StatusNotFound,"Student not found")
			default :
			    httpResp.RespondWithError(w, http.StatusInternalServerError,err.Error())
}}else{
	httpResp.ResponseWithJson(w, http.StatusOK, stud)
}}

func DeleteStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){                     //if true this if code won't execute so use !VerifyCookie which is false
		return  
	}
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdId: stdId}
	if err := s.Delete(); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})

}

// getall (get request)
func GetAllStuds(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){                     //if true this if code won't execute so use !VerifyCookie which is false
		return  
	}
	students, getErr := model.GetALLStudents()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK,students)
}
// //*************COURSE******
// func AddCourse(w http.ResponseWriter, r *http.Request) {	
// 	var cour model.Course	
// 	decoder := json.NewDecoder(r.Body)	

// if err := decoder.Decode(&cour); err != nil {
// response, _ := json.Marshal(map[string]string{"error": "invalid json body"})
// w.Header().Set("Content-Type", "application/json")
// w.WriteHeader(http.StatusBadRequest)
// w.Write(response)
// return
// }
// defer r.Body.Close()
// saveErr := cour.Create()
// if saveErr != nil {
// response, _ := json.Marshal(map[string]string{"error":
// saveErr.Error()})
// w.Header().Set("Content-Type", "application/json")
// w.WriteHeader(http.StatusBadRequest)
// w.Write(response)
// return
// }
// // no error
// response, _ := json.Marshal(map[string]string{"status": "course added"})
// w.Header().Set("Content-Type", "application/json")
// w.WriteHeader(http.StatusCreated)
// w.Write(response)
// }
