package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"
)

var admin model.Admin
func Signup( w http.ResponseWriter, r *http.Request){

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin);err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr:= admin.Create()
	if saveErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// //no error
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}
func Login(w http.ResponseWriter, r *http.Request){
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	getErr := admin.Get()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return 
	}
	// set cookie
	cookie := http.Cookie{
		Name:"my-cookie",
		Value:"my-cookie trail",                                          //admin.Email
		Expires: time.Now().Add(30 * time.Minute),
		Secure:true,
	}
	//set cookie and send back to client
	http.SetCookie(w, &cookie)
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "Success"})
}

// logout
func LogOut(w http.ResponseWriter, r *http.Request){
	http.SetCookie(w, &http.Cookie{
		Name:"my-cookie",
		Expires : time.Now(),

	})
	httpResp.ResponseWithJson(w, http.StatusOK,map[string]string{"message":"logout success"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool{
	cookie,err := r.Cookie("my-cookie")
	if err != nil{
		if err == http.ErrNoCookie{
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not set")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError,"internal server error")
		return false
	}
	// validate the check
	if cookie.Value != "my-cookie trail"{
		httpResp.RespondWithError(w, http.StatusSeeOther,"Cookie value does not match")
		return false
	}
	return true
}