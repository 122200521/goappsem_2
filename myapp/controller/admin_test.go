package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// testing is standard go build in package-

func TestAdmExists(t *testing.T){
	url := "http://localhost:8080/login"
	var data = []byte(`{"email":"12220052.gcit@rub.edu.bt","password":"applenmango"}`)
	// req is the request object
	req,_:= http.NewRequest("POST",url,bytes.NewBuffer(data))
	req.Header.Set("Content-Type","application/json")
	// to send request we need a client
	client:=&http.Client{}
	resp,err :=client.Do(req) // sends request using Do function
	if err != nil{
			panic(err)

	}
	defer resp.Body.Close() //defer is the last code to run (executes when the function is about to terminate) 
	body,_:= io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK,resp.StatusCode)
	expRes := `{"message":"Success"}`
	assert.JSONEq(t, expRes,string(body))
}

func TestAdmLoginUserNotExit(t *testing.T){
	url := "http://localhost:8080/Login"
	var data = []byte(`{"email" :"12220052@rub.edu.bt","password":"applenmango"}`)
	//create request object
	req,_:= http.NewRequest("POST", url,bytes.NewBuffer(data))
	req.Header.Set("Content-Type","application/json")
	// create client
	client := &http.Client{}
	resp,err := client.Do(req)
	if err != nil{
		panic(err)
	}
	defer resp.Body.Close()
	body,_:= io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusUnauthorized,resp.StatusCode)
	assert.JSONEq(t,`{"Error":"sql: no rows in result set"}`,string(body))
}