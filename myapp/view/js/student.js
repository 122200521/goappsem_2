// window.onload=function(){
//   fetch('/students')
//       .then(response=>response.text())
//       .then(data => showStudent(data))
// }
// function addStudent() {
//   var data = getFormdata();
//   var sid = data.stdid;
//   if (isNaN(sid)) {
//     alert("Enter valid student")
//     return
//   } else if (data.email ==""){
//     alert( "Email cannot be empty") 
//     return
//   } else if(data.fname==""){
//     alert("first name cannot be empty")
//     return
//   }
  
//   // like a http request

//   fetch("/student", {
//     method: "POST",
//     body: JSON.stringify(data), //conery js data to json
//     headers: { "Content-type": "application/json; charset=UTF-8" },
//   }).then((response1) => {
//     if (response1.ok) {
//       fetch("/student/" + sid)
//         .then((response2) => response2.text())
//         .then((data) => showStudent(data));
//     } else{
//       throw new Error(response1.statusText)
//     } 
//   }).catch(e =>{
//     alert(e)
//   });
//   resetform();
// }

// function showStudent(data) {
//   const student = JSON.parse(data);
//   newRow(student)
// }

// function resetform(){
//   document.getElementById("sid").value= "";
//   document.getElementById("fname").value= "";
//   document.getElementById("lname").value= "";
//   document.getElementById("email").value= "";
// }
// function showStudents(data){
//   const students = JSON.parse(data)
//   students.forEach(stud=>{
//     newRow(stud)
//   })
//  }

// //  reducing duplicate helper function--- new row for table we have  and then we cannot directly ender data 
//  function newRow(student){
//   var table=document.getElementById("myTable");
//     var row= table.insertRow(table.length);
//     var td=[]
//     for (i = 0; i < table.rows[0].cells.length; i++) {
//       td[i] = row.insertCell(i);
//     }
//     td[0].innerHTML = student.stdid;
//     td[1].innerHTML = student.fname;
//     td[2].innerHTML = student.lname;
//     td[3].innerHTML = student.email;
//     td[4].innerHTML='<input type = "button" onclick = "deleteStudent(this)" value="delete" id ="button-1">'
//     td[5].innerHTML='<input type = "button" onclick = "updateStudent(this)" value="edit" id ="button-2">'
//  }


// function updateStudent(r){
//   selectedRow = r.parentElement.parentElement
//   document.getElementById("sid").value = selectedRow.cells[0].innerHTML
//   document.getElementById("fname").value = selectedRow.cells[1].innerHTML
//   document.getElementById("lname").value = selectedRow.cells[1].innerHTML
//   document.getElementById("email").value = selectedRow.cells[1].innerHTML

//   var btn = document.getElementById("button-add")
//   sid = selectedRow.cells[0].innerHTML

//   if (btn) {
//     btn.innerHTML = "update"
//     btn.setAttribute("onclick","update(sid)")
//   }

// }

// function getFormdata(){
//   var formdata = {
//     stdid: parseInt(document.getElementById("sid").value),
//     fname: document.getElementById("fname").value,
//     lname: document.getElementById("lname").value,
//     email: document.getElementById("email").value,
//   }
//   return formdata
// }
// function update(sid){
//   var newData= getFormdata()

//     fetch ('/student/' + sid,{
//         method: "PUT",
//         body: JSON.stringify(newData), //conery js data to json
//         headers: { "Content-type": "application/json; charset=UTF-8" }
//     }
//   ).then(res => {
//     if (res.ok){
//       selectedRow.cells[0].innerHTML = newData.stdid;
//       selectedRow.cells[1].innerHTML = newData.fname;
//       selectedRow.cells[2].innerHTML = newData.lname;
//       selectedRow.cells[3].innerHTML = newData.email;

//       var btn = document.getElementById("button-add")
//       // sid = selectedRow.cells[0].innerHTML
    
//       if (btn) {
//         btn.innerHTML = "Add"
//         btn.setAttribute("onclick","addStudent()")
//         selectedRow = null;

//         resetform();
//       }

//     }else{
//       alert("server: update request error")
//     }
//   })
// }

window.onload = function(){
  fetch('/students')
    .then(response => response.text())
    .then(data => showStudents(data))
}

function addStudent() {
var data= getformData()
    var data = {
      stdid: parseInt(document.getElementById("sid").value),
      fname: document.getElementById("fname").value,
      lname: document.getElementById("lname").value,
      email: document.getElementById("email").value,
    };
    // if (isNaN(sid)){
    //   alert("Enter valid student ID")
    //   return
    //  }else if(data.email == ""){
    //   alert("Email cannot be empty")
    //   return
    //  }else if(data.fname == ""){
    //   return
    //  }
    var sid = data.stdid;
    fetch("/student", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-type": "application/json;charset-UTF-8" },
    }).then((response1) => {
      if (response1.ok) {
        fetch("/student/"+sid)
          .then((response2) => response2.text())
          .then((data) => showStudent(data));
      }else{
        throw new Error(response1.statusText)
      }
    }).catch(e=> {
      alert(e)
    });
    resetform();
  }

function showStudent(data){
    const student = JSON.parse(data)
    newRow(student)
  
   }
   function resetform(){
    document.getElementById("sid").value = "";
    document.getElementById("fname").value = "";
    document.getElementById("lname").value = "";
    document.getElementById("email").value = "";
   }

   function showStudents(data){
    const students = JSON.parse(data)
    students.forEach(stud =>{
      newRow(stud)
   });
  }
//add new row to the table
   function newRow(student){
    var table = document.getElementById("myTable")
    var row = table.insertRow(table.length)
    var td=[]
        for (i =0;i<table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i)
    }
      td[0].innerHTML=student.stdid
      td[1].innerHTML=student.fname
      td[2].innerHTML=student.lname
      td[3].innerHTML=student.email
      td[4].innerHTML='<input type = "button" onclick = "deleteStudent(this)" value="delete" id ="button-1">'
      td[5].innerHTML='<input type = "button" onclick = "updateStudent(this)" value="edit" id ="button-2">'
  
  }
   
var selectedRow


  function updateStudent(r){
    selectedRow= r.parentElement.parentElement;
    //fill in the form fields with selected row data
    document.getElementById("sid").value = selectedRow.cells[0].innerHTML
    document.getElementById("fname").value = selectedRow.cells[1].innerHTML
    document.getElementById("lname").value = selectedRow.cells[2].innerHTML
    document.getElementById("email").value = selectedRow.cells[3].innerHTML


    var btn = document.getElementById("button-add")
    sid=selectedRow.cells[0].innerHTML;
    if (btn){
      btn.innerHTML = "Update";
      btn.setAttribute("onclick", "update(sid)")
    }
  }

//helper function
function getformData(){
  var formData={
    stdid: parseInt(document.getElementById("sid").value),
    fname: document.getElementById("fname").value,
    lname: document.getElementById("lname").value,
    email: document.getElementById("email").value,
  }
  return formData
}


  function update(sid){
    var newData= getformData()
    fetch('/student/' +sid,{
      method: "PUT",
      body: JSON.stringify(newData),
      headers: { "Content-type": "application/json;charset-UTF-8" },
    })
    .then(res =>{
      if (res.ok){
        selectedRow.cells[0].innerHTML=newData.stdid;
        selectedRow.cells[1].innerHTML=newData.fname;
        selectedRow.cells[2].innerHTML=newData.lname;
        selectedRow.cells[3].innerHTML=newData.email;

        var btn = document.getElementById("button-add")
        if (btn){
          btn.innerHTML = "Add";
          btn.setAttribute("onclick", "addStudent()")
          selectedRow=null;
        }
        else{
          alert("server: update request error")
        }
      }

    })
  }

function deleteStudent(r){
  if (confirm('Are you sure you want to DELETE this')){
    // assign in the selectedRow the parentElement of the parentElement of r this(input)-> tr->td
    selectedRow = r.parentElement.parentElement;
    // row data of the student you want to delete
    sid = selectedRow.cells[0].innerHTML;
    fetch('/student/' + sid,{
      method:"DELETE",
      headers:{"Content-type": "application/json;charset-UTF-8"}
    });

    // It assigns to the variable 'rowIndex' the index of the row in the HTML table that is referenced by the 'selectedRow' variable.
    // It checks if the index of the row is greater than 0 (i.e., if it's not the header row of the table).
    // If the row index is greater than 0, it deletes the row with that index from the HTML table with the ID "myTable".
    // It sets the 'selectedRow' variable to null to clear the reference to the deleted row

    var rowIndex = selectedRow.rowIndex;
    if (rowIndex>0){
      document.getElementById("myTable").deleteRow(rowIndex);
    }
    selectedRow = null;
  }
}