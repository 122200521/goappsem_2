package controller

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// // define handler func to handle incoming request
func HomeHandler(w http.ResponseWriter,r *http.Request){
	_,err:=w.Write([]byte("hello World"))
	if err!= nil{
	 fmt.Println("error:",err)
	}
 }
 func ParameterHandler(w http.ResponseWriter, r *http.Request){
	 para:=mux.Vars(r)
	 name:=para["myname"]
	 // fmt.Println(name)
	 _,err:=w.Write([]byte("My name is " + name))
	 if err !=nil{
		   os.Exit(1)
	 }
 }
