package model

import (
	"myapp/datastore/postgres"
)


type Student struct {
	StdId int64 `json:"stdid"`
	FirstName string `json:"fname"`
	LastName string `json:"lname"`
	Email string `json:"email"`

}


const (queryInsert ="INSERT INTO student(stdid,firstname,lastname,email) VALUES($1,$2,$3,$4);"
)
// // communicate with database(add student data to database)
func (s *Student) Create() error{
	// fmt.Println(s)
_,err :=postgres.Db.Exec(queryInsert,s.StdId,s.FirstName,s.LastName,s.Email)
// // we use .Exec when we don't want anything to be returned
return err 
//   return nil 
}


const ( queryGetUser = "SELECT stdid,firstname,lastname,email FROM student WHERE stdid=$1;"
		queryUpdate = "UPDATE student SET stdid = $1, firstname= $2, lastname=$3, email=$4 WHERE stdid = $5 RETURNING stdid;"


)
func (s *Student) Read() error {
	return postgres.Db.QueryRow(queryGetUser,s.StdId).Scan(&s.StdId,&s.FirstName, &s.LastName,&s.Email)
// // we  use postgres.Db.QueryRow when we want onlu one row. if we want more rows use postgres.Db.QueryRows
}


// // Update
func (s *Student) Update(oldID int64) error {
	err:=postgres.Db.QueryRow(queryUpdate, s.StdId, s.FirstName, s.LastName, s.Email, oldID).Scan(&s.StdId)
	return err
}

// // Delete
const queryDeleteUser = "Delete FROM student WHERE stdid=$1 RETURNING stdid;"
func (s *Student)Delete()error{
	if  err := postgres.Db.QueryRow(queryDeleteUser, s.StdId).Scan(&s.StdId); err !=nil{
		return err
	}
	return nil
}

// Read all rows
func GetALLStudents() ([]Student, error){ // no particular receiver because we need a set of students not particular
	rows, err :=postgres.Db.Query("SELECT * FROM Student;") // directly passing from here
	// error handling
	if err != nil{
		return nil, err
	}
//    slice of type student
	students := []Student{} // empty slice of type student

	// iterate rows
	for rows.Next(){  // next iterates rows one by one
		var s Student
		dbErr:=rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email) // scan - store the values in s
		if dbErr != nil{
			return nil, dbErr
		}
		students=append(students, s) // adding data in []student using append
	}

	rows.Close()
    return students, nil
}





	