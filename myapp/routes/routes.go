package routes

import (
	"log"
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func IntiializeRoutes(){
	////create a new router
	router:=mux.NewRouter()
	// // register handler function with mux router
	router.HandleFunc("/home",controller.HomeHandler)
	// // new router 
	router.HandleFunc("/urlParameters/{myname}", controller.ParameterHandler)
	// //student
	router.HandleFunc("/student",controller.AddStudent).Methods("POST")
	// // to read
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	// // to Update
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	// // to DELETE
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")
	// // to Read all
	router.HandleFunc("/students", controller.GetAllStuds)


	// // COURSE
	router.HandleFunc("/course", controller.AddCourse).Methods("POST")
	router.HandleFunc("/course/{cid}", controller.GetCour).Methods("GET")
	router.HandleFunc("/course/{cid}", controller.UpdateCour).Methods("PUT")
	router.HandleFunc("/course/{cid}", controller.DeleteCour).Methods("DELETE")
	router.HandleFunc("/courses",controller.GetAllCour)

	//// Sign up
	router.HandleFunc("/signup",controller.Signup).Methods("POST")

	// //Login
	router.HandleFunc("/login",controller.Login).Methods("POST")
	router.HandleFunc("/logout",controller.LogOut).Methods("GET")

// enroll
router.HandleFunc("/enroll", controller.Enroll).Methods("POST")
router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")
router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("DELETE")


	

	//  to serve static file
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
	// // start http server
	log.Println("Application running on port 8080")

	err := http.ListenAndServe(":8080",router)
	if err != nil{
		os.Exit(1)
	}
	
}
