package httpResp

import (
	"encoding/json"
	"net/http"
)

func RespondWithError(w http.ResponseWriter, code int, message string){
	ResponseWithJson(w, code, map[string]string {"Error": message})//if there is error
}

func ResponseWithJson(w  http.ResponseWriter, code int, payload interface{}){//map is there in payload
	// res_map:= map[string]string{"error": dbErr.Error()}
	response, _:=json.Marshal(payload)
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)//ccode is code int status code
	// return//programe terminates
	w.Write(response)
	//
}